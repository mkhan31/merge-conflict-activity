public class RightTriangle
{
    private int base;
    private int height;
    private int hypo;

    public RightTriangle(int s1, int s2, int s3)
    {
        this.base = s1;
        this.height = s2;
        this.hypo = s3;
    }

    public double getArea()
    {
        return ((this.base*this.height)/2.0);
    }

    public String toString()
    {
        return "Side 1: " + this.base + "\nSide 2: " + this.height + "\nSide 3: " + this.hypo;
    }
}
