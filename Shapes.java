public class Shapes
{
    public static void main(String[] args)
    {

        //tests
        Rectangle test = new Rectangle(2,5);
        System.out.println(test.getArea());
        System.out.println(test.getPerimeter());
        System.out.println(test);

        RightTriangle triangle = new RightTriangle(3, 4, 5);
        System.out.println(triangle);
        System.out.println("Area: " + triangle.getArea());

    }
}
